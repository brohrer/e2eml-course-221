"""
Use k-nearest neighbors to identify penguins.
The data is downloaded from
https://raw.githubusercontent.com/mcnakhaee/palmerpenguins/master/palmerpenguins/data/penguins.csv
and the data set is metculously documented here
https://github.com/allisonhorst/palmerpenguins/blob/master/README.md

Data citation:
   Horst AM, Hill AP, Gorman KB (2020). palmerpenguins: Palmer
   Archipelago (Antarctica) penguin data. R package version 0.1.0.
   https://allisonhorst.github.io/palmerpenguins/. doi:
   10.5281/zenodo.3960218.

License: CC0 Public Domain
"""
import os


def load_data():
    """
    Open and parse the csv file containing the penguin data.
    """
    data_filename = os.path.join("data", "penguins.csv")

    with open(data_filename, "rt") as f:
        data_lines = f.readlines()
        # The first row is full of column labels.
        # column_labels = data_lines[0].split(",")

        # For each penguin, split the line up by commas, ignore
        # any residual whitespace on the ends, and pull out
        # the feature and label fields.
        # Start from row 1 so as to skip the column headings.
        for line in data_lines[1:]:
            line_data = line.split(",")
            numerical_data = line_data[2:6]
            sex_feature = line_data[6]
            label = line_data[0]

            print(numerical_data, sex_feature, label)


if __name__ == "__main__":
    load_data()
