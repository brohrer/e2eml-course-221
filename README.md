# End to End Machine Learning Course 221, k-nearest neighbors

An introduction to k-nearest neighbors using examples of classification, regression, and interpolation.

Check out the full course [here](https://e2eml.school/221).

## Data sets

### Penguins

[Original R package](https://github.com/allisonhorst/palmerpenguins/blob/master/README.md)

```
Horst AM, Hill AP, Gorman KB (2020).
palmerpenguins: Palmer Archipelago (Antarctica) penguin data.
R package version 0.1.0.
https://allisonhorst.github.io/palmerpenguins/.
doi: 10.5281/zenodo.3960218.
```

License: CC0 Public Domain

[csv version on Kaggle](https://www.kaggle.com/parulpandey/palmer-archipelago-antarctica-penguin-data?select=penguins_size.csv)

[csv verson on GitHub](https://github.com/mcnakhaee/palmerpenguins/blob/master/palmerpenguins/data/penguins.csv)
