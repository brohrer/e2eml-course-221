"""
Use k-nearest neighbors to estimate the price of diamonds.
The data is downloaded from Kaggle
https://www.kaggle.com/shivam2503/diamonds
"""
import os
import numpy as np

data_filename = os.path.join("data", "diamonds.csv")


def load_data():
    """
    Retrieve the diamonds data from a csv file, pull out the useful bits,
    and convert it all to numbers.

    Returns
    features, 2D NumPy array of floats
    labels, 1D NumPy array of floats
    """
    # column 0 is row number
    # column 7 is price

    # Create ordered lists for cut, color, and clarity, so that their values
    # can be converted into numbers based on their position in the list.
    # column 2 cut
    col2_labels = ['"Fair"', '"Good"', '"Very Good"', '"Premium"', '"Ideal"']
    # column 3 color
    col3_labels = ['"J"', '"I"', '"H"', '"G"', '"F"', '"E"', '"D"']
    # column 4 clarity
    col4_labels = [
        '"I1"', '"SI2"', '"SI1"', '"VS2"', '"VS1"', '"VVS2"', '"VVS1"', '"IF"']

    with open(data_filename, "rt") as f:
        data_lines = f.readlines()
        # column_labels = data_lines[0].split(",")

        raw_data = []
        # Ignore the row with the column labels.
        for line in data_lines[1:]:
            line_strings = line.split(",")
            # Convert cut, color, and clarity strings into numbers, based
            # on their position in their respective lists.
            line_strings[2] = col2_labels.index(line_strings[2])
            line_strings[3] = col3_labels.index(line_strings[3])
            line_strings[4] = col4_labels.index(line_strings[4])
            # Leave off the row number by starting at column 1.
            # Convert everything to floats.
            line_nums = [float(val) for val in line_strings[1:]]
            raw_data.append(line_nums)
        raw_data = np.array(raw_data)

        # Segregate out the features from the prices.
        features = raw_data[:, [0, 1, 2, 3, 4, 5, 7, 8, 9]]
        labels = raw_data[:, 6]
        return features, labels


if __name__ == "__main__":
    features, labels = load_data()

    print(features[:15, :])
    print(labels[:15])
