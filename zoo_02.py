"""
Use k-nearest neighbors to classify zoo animals.
The data is downloaded from the UCI Machine Learning
https://archive.ics.uci.edu/ml/datasets/Zoo

Data source citation:
    Dua, D. and Graff, C. (2019).
    UCI Machine Learning Repository [http://archive.ics.uci.edu/ml].
    Irvine, CA: University of California,
    School of Information and Computer Science.

Original data created and donated by Richard S. Forsyth
For more background information:
http://www.richardsandesforsyth.net/beagling.html
"""
import os
import numpy as np

# Setting the random seed ensures that you'll get the same results each time
# you run this.
np.random.seed(645839)

# The fraction of the data set that will be set aside to "train" the k-NN model
training_fraction = .5

# The number of times to calculate a new train/test split and re-run the
# evaluation
n_splits = 100

data_filename = os.path.join("data", "zoo.data")


def main():
    """
    Try different feature weights to see what gives the best performance.
    """
    features, labels = load_data()
    # n_animals = features.shape[0]
    # n_features = features.shape[1]

    for k in [1, 3, 5, 7, 9]:
        scores = []
        # Repeat the split-and-test process many times and find the
        # average performance for this set of weights.
        for i in range(n_splits):
            (train_features,
                train_labels,
                test_features,
                test_labels) = prep_data(features, labels)
            n_test = test_labels.size
            total_score = 0

            # Evaluate this particular train-test split with this
            # particular set of weights.
            for i_test in range(n_test):
                i_top, distances = find_k_nearest(
                    k, train_features, test_features[i_test, :])
                score = score_examples(
                    distances, train_labels[i_top], test_labels[i_test])
                total_score += score
            # Find the average score of the test data.
            scores.append(total_score / n_test)

        # Find the average test score across many splits.
        overall_score = np.mean(scores)

        print("k", k, "score", overall_score)


def load_data():
    """
    Open and parse the csv file.

    Returns
    features, 2D NumPy array of floats
        Each row represents a data point and each column a feature.
    labels, 1D NumPy array of floats
        There will be one element per data point. The number will represent
        the category.
    """

    with open(data_filename, "rt") as f:
        data_lines = f.readlines()
        # column_labels = data_lines[0].split(",")

        # For each row, split the line up by commas,
        # convert each element to a float,
        # and pull out the feature and label fields.
        # Start from row 1 so as to skip the column headings.
        raw_data = []
        for line in data_lines[1:]:
            line_strings = line.split(",")
            line_nums = [float(val) for val in line_strings[1:]]
            raw_data.append(line_nums)

        raw_data = np.array(raw_data)
        features = raw_data[:, :-1]
        # The labels are in the last column of the array.
        labels = raw_data[:, -1]
        return features, labels


def prep_data(features, labels):
    """
    Split the features and labels into training and testing groups.
    Shift and scale to have a mean of zero and a variance of 1.

    Adjust the global variable training_fraction to control how much
    information the algorithm has to work with. Lower values are more
    challenging.

    Arguments
    features, 2D NumPy array of floats
    labels, 1D NumPy array of floats

    Returns
    train_features, 2D NumPy array of floats
    train_labels, 1D NumPy array of floats
    test_features, 2D NumPy array of floats
    test_labels, 1D NumPy array of floats
    """
    n_pts = features.shape[0]
    n_train = int(n_pts * training_fraction)

    # Divide up the data by generating a set of "straws"
    # of different length and distributing them randomly.
    # The data points with the shortest straws are the training set.
    straws = np.arange(n_pts)
    np.random.shuffle(straws)
    i_train = straws[:n_train]
    i_test = straws[n_train:]

    train_features = features[i_train, :]
    test_features = features[i_test, :]
    train_labels = labels[i_train]
    test_labels = labels[i_test]

    return train_features, train_labels, test_features, test_labels


def find_k_nearest(k, train_points, test_point):
    """
    Find the distance between the a single test point and
    each of the training points.
    Return the indices of the nearest neighbors.

    Arguments
    train_points, 2D NumPy array of floats
    test_point, 1D NumPy array of floats

    Returns
    i_top_k, 1D NumPy array of floats
    distance_top_k, 1D NumPy array of floats
    """
    distance = 1 / np.sum(train_points == test_point[np.newaxis, :], axis=1)
    order = np.argsort(distance)
    i_top_k = order[:k]
    distance_top_k = distance[i_top_k]
    return i_top_k, distance_top_k


def score_examples(distances, labels, actual):
    """
    Let the k-nearest data points vote on the predicted category.
    Each gets a vote that is the reciprocal of its distance to the test point.
    A correct predcition gets full credit, except
    in case of a tie, a correct prediction only gets half-credit.

    Arguments
    distances, 1D NumPy array of floats
    labels, 1D NumPy array of floats
    actual, float

    Returns
    credit, float
    """
    # Create a set of ballot boxes.
    predictions = np.zeros(int(np.max(labels) + 1))
    # Collect the votes from each neighbor, weighted by distance.
    for i_label, label in enumerate(labels):
        predictions[int(label)] += 1 / distances[i_label]
    # Tally them up.
    max_vote = np.max(predictions)
    predicted_label_index = np.where(predictions == max_vote)[0]

    if predicted_label_index.size > 1:
        # In case of a tie prediction
        credit = .5
    else:
        credit = 1

    # Was the prediction(s) correct?
    if actual in predicted_label_index:
        return credit
    else:
        return 0


if __name__ == "__main__":
    main()
