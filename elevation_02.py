"""
Data downloaded from
Staged Products Directory
The National Map
United States Geological Survey
US Department of the Interior
https://prd-tnm.s3.amazonaws.com/index.html?prefix=StagedProducts/Elevation/1m/Projects/NM_CO_Southern_San_Luis_TL_2015/TIFF/

The data covers a square parcel, a little over 10 km on a side.
Elevation is reported every meter in both the East-West direction (columns)
and the North-South direction (rows).
Locations missing data have a physically nonsensical
large negative number assigned to them.
"""
import os
import numpy as np
import rasterio

# The number of rows and columns in the reconstructed version of the
# elevation data.
reconstructed_size = 2 ** 4

geotiff_filename = os.path.join(
    "data", "USGS_one_meter_x46y404_NM_CO_Southern_San_Luis_TL_2015.tif")


def main():
    """
    Read in the elevation data, downsample it, reconstruct it, visualize it.
    """
    full_elevation = read_elevation_data()

    # Do some downsampling.
    n_rows, n_cols = full_elevation.shape
    # Find the row and column indices that will constitute the
    # downsampled grid.
    y_recon = [int(x) for x in np.linspace(0, n_rows - 1, reconstructed_size)]
    x_recon = [int(x) for x in np.linspace(0, n_cols - 1, reconstructed_size)]

    # Expand the row and column locations to a full grid.
    X, Y = np.meshgrid(x_recon, y_recon)
    reconstructed_elevation = full_elevation[Y, X]
    print(reconstructed_elevation)


def read_elevation_data():
    """
    Import the data from the geotiff file provided.
    The rasterio package handles geotiffs nicely.
    Replace instances of missing data with the lowest elevation present
    in the non-missing data.

    The elevation data is in meters, and is sampled at 1 meter intervals
    in both directions.

    Returns
    elevation, a 2D NumPy array of floats
    """
    elevation = None
    with rasterio.open(geotiff_filename) as src:
        # By default, the data returned is in a 3D array, but this geotiff
        # only has one channel.
        # Explicity reduce the 0th channel to a 2D array.
        elevation = src.read()[0, :, :]

        # Find the indices of the missing data.
        i_undefined = np.where(elevation < 0)
        # Change the missing data to a physically implausible large value.
        elevation[i_undefined] = 1e6
        # Find the lowest value of non-missing elevation.
        elevation_floor = np.min(elevation)
        # Change the missing data to match that.
        elevation[i_undefined] = elevation_floor
    return elevation


if __name__ == "__main__":
    main()
