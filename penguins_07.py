"""
Use k-nearest neighbors to identify penguins.
The data is downloaded from
https://raw.githubusercontent.com/mcnakhaee/palmerpenguins/master/palmerpenguins/data/penguins.csv
and the data set is metculously documented here
https://github.com/allisonhorst/palmerpenguins/blob/master/README.md

Data citation:
   Horst AM, Hill AP, Gorman KB (2020). palmerpenguins: Palmer
   Archipelago (Antarctica) penguin data. R package version 0.1.0.
   https://allisonhorst.github.io/palmerpenguins/. doi:
   10.5281/zenodo.3960218.

License: CC0 Public Domain
"""
import os
import numpy as np


def main():
    features, labels = load_data()
    print("number of features", features.shape[1])
    print("number of data points", features.shape[0])
    print("number of labels", labels.size)


def load_data():
    """
    Open and parse the csv file containing the penguin data.

    Returns
    features, 2D NumPy array of floats
        Each row represents a penguin and each column a feature.
    labels, 1D NumPy array of floats
        There will be one element per penguin. The number will represent
        the type of penguin.
    """
    data_filename = os.path.join("data", "penguins.csv")

    with open(data_filename, "rt") as f:
        data_lines = f.readlines()
        # The first row is full of column labels.
        # column_labels = data_lines[0].split(",")

        n_penguins = len(data_lines) - 1
        n_features = 5
        features = np.zeros((n_penguins, n_features))
        labels = np.zeros(n_penguins)

        # Describe how to convert sex and species text fields to numbers.
        sex_conversion = {"male": 0, "female": 1}
        label_conversion = {
            "Adelie": 0,
            "Chinstrap": 1,
            "Gentoo": 2,
        }

        # For each penguin, split the line up by commas, ignore
        # any residual whitespace on the ends, and pull out
        # the feature and label fields.
        # Start from row 1 so as to skip the column headings.
        # The try-except block catches the cases where the data is
        # missing and is replaced with and "NA". For now, we're choosing to
        # ignore all these data points.
        for i_penguin, line in enumerate(data_lines[1:]):
            line_data = line.split(",")
            try:
                numerical_data = [float(x.rstrip()) for x in line_data[2:6]]
                features[i_penguin, :4] = numerical_data
                # Convert sex and species to numbers.
                features[i_penguin, 4] = sex_conversion[line_data[6].rstrip()]
                labels[i_penguin] = label_conversion[line_data[0].rstrip()]
            except ValueError:
                # If any NA's are encountered in the numerical fields
                # just move along to the next penguin.
                pass
            except KeyError:
                # If any NA's are encountered in the sex conversion
                # just move along to the next penguin.
                pass

        return features, labels


if __name__ == "__main__":
    main()
