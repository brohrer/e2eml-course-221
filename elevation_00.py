"""
Data downloaded from
Staged Products Directory
The National Map
United States Geological Survey
US Department of the Interior
https://prd-tnm.s3.amazonaws.com/index.html?prefix=StagedProducts/Elevation/1m/Projects/NM_CO_Southern_San_Luis_TL_2015/TIFF/

The data covers a square parcel, a little over 10 km on a side.
Elevation is reported every meter in both the East-West direction (columns)
and the North-South direction (rows).
Locations missing data have a physically nonsensical
large negative number assigned to them.
"""
import os
import numpy as np
import rasterio

geotiff_filename = os.path.join(
    "data", "USGS_one_meter_x46y404_NM_CO_Southern_San_Luis_TL_2015.tif")


def main():
    """
    Read in the elevation data, downsample it, reconstruct it, visualize it.
    """
    full_elevation = read_elevation_data()
    print(full_elevation[:10, :10])
    print(np.min(full_elevation))


def read_elevation_data():
    """
    Import the data from the geotiff file provided.
    The rasterio package handles geotiffs nicely.

    The elevation data is in meters, and is sampled at 1 meter intervals
    in both directions.

    Returns
    elevation, a 2D NumPy array of floats
    """
    elevation = None
    with rasterio.open(geotiff_filename) as src:
        # By default, the data returned is in a 3D array, but this geotiff
        # only has one channel.
        # Explicity reduce the 0th channel to a 2D array.
        elevation = src.read()[0, :, :]
    return elevation


if __name__ == "__main__":
    main()
