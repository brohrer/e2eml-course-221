"""
Data downloaded from
Staged Products Directory
The National Map
United States Geological Survey
US Department of the Interior
https://prd-tnm.s3.amazonaws.com/index.html?prefix=StagedProducts/Elevation/1m/Projects/NM_CO_Southern_San_Luis_TL_2015/TIFF/

The data covers a square parcel, a little over 10 km on a side.
Elevation is reported every meter in both the East-West direction (columns)
and the North-South direction (rows).
Locations missing data have a physically nonsensical
large negative number assigned to them.
"""
import os
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D  # noqa F401

# The number of rows and columns in the down-sampled version of the
# elevation data.
sampled_size = 2 ** 5

# The number of rows and columns in the reconstructed version of the
# elevation data.
reconstructed_size = 2 ** 7

elevation_filename = os.path.join("data", "downsampled_elevation.npy")
reconstructed_image_filename = "reconstructed_elevation.png"

k = 9


def main():
    """
    Read in the elevation data, downsample it, reconstruct it, visualize it.
    """
    full_elevation = read_elevation_data()
    sampled_elevation = get_samples(full_elevation)
    (x_recon, y_recon, reconstructed_elevation) = \
        reconstruct(sampled_elevation)
    visualize(x_recon, y_recon, reconstructed_elevation)


def read_elevation_data():
    """
    The elevation data is in meters, and is sampled
    at about 10 meter intervals in both directions.
    Replace instances of missing data with the lowest elevation present
    in the non-missing data.

    Returns
    elevation, a 2D NumPy array of floats
    """
    elevation = np.load(elevation_filename)

    # Find the indices of the missing data.
    i_undefined = np.where(elevation < 0)
    # Change the missing data to a physically implausible large value.
    elevation[i_undefined] = 1e6
    # Find the lowest value of non-missing elevation.
    elevation_floor = np.min(elevation)
    # Change the missing data to match that.
    elevation[i_undefined] = elevation_floor
    return elevation


def get_samples(elevation):
    """
    Downsample the original data to evenly spaced rows and columns.

    The original data is a 2D array of elevation values in meters.
    It's understood that each row index and column index does double
    duty representing that element's location in meters. This is thanks
    to the convenient fact that the data was collected at a 1 meter
    resolution.

    The downsampled data won't be represented so conveniently. In fact,
    we'd like to choose a representation that doesn't depend on a
    regular grid at all. To get this, we'll use a 2D array where each
    row represents one measurement, the 0th column is its x position,
    the 1st column is its y position, and the 2nd column is its
    elevation. This N x 3 sampled representation could be used to
    describe any spacing, regular or irregular.

    Arguments
    elevation, a 2D NumPy array of floats

    Returns
    sampled_elevation, a 2D NumPy array of floats
    """
    # Forgive the index finagling here.
    # All this is to construct the indices for downsampling.
    n_rows, n_cols = elevation.shape
    # Find the row and column indices that will constitute the
    # downsampled grid.
    i_rows = [int(x) for x in np.linspace(0, n_rows - 1, sampled_size)]
    i_cols = [int(x) for x in np.linspace(0, n_cols - 1, sampled_size)]
    # Generate all the x-y pairs of the grid points.
    xx, yy = np.meshgrid(i_rows, i_cols)
    x_samples = xx.ravel()
    y_samples = yy.ravel()
    # Perform the dowsampling.
    # y-coordinate corresponds to rows and x to columns.
    elevation_values = elevation[y_samples, x_samples]

    # Assemble x, y, and elevation for each sample into a N x 3 array.
    sampled_elevation = np.concatenate((
        x_samples[:, np.newaxis],
        y_samples[:, np.newaxis],
        elevation_values[:, np.newaxis]), axis=1)

    return sampled_elevation


def reconstruct(sampled_elevation):
    """
    Reconstruct the image using the samples.
    The reconstructed image is known to be a grid, but unlike the
    original data, the rows and columns aren't in 1 meter increments.
    To capture this, the positions of each row and column of estimates
    in meters is returned, together with the array of interpolated
    elevations.

    See get_samples() for details on the structure of the
    input argument.

    Arguments
    sampled_elevation, a 2D NumPy array of floats

    Returns
    x_recon, a 1D NumPy array of floats
    y_recon, a 1D NumPy array of floats
    reconstructed_elevation, a 2D NumPy array of floats
    """
    # Find the x- and y-positions in meters of the columns and rows
    # of the reconstruction.
    x_max = np.max(sampled_elevation[:, 0])
    y_max = np.max(sampled_elevation[:, 1])
    x_recon = np.linspace(0, x_max, reconstructed_size, dtype=int)
    y_recon = np.linspace(0, y_max, reconstructed_size, dtype=int)

    # Create an empty reconstruction of the right size.
    reconstructed_elevation = np.zeros(
        (reconstructed_size, reconstructed_size))

    # Make sure, for very coarse reconstructions, that k doesn't exceed
    # the size of the downsampled data.
    k_corrected = np.minimum(sampled_elevation.shape[0], k)

    interpolate(
        k_corrected,
        x_recon,
        y_recon,
        sampled_elevation,
        reconstructed_elevation)
    return x_recon, y_recon, reconstructed_elevation


def interpolate(
    k,
    x_recon,
    y_recon,
    sampled_elevation,
    reconstructed_elevation
):
    """
    Use the samples to find the estimated elevation.

    Arguments
    k, int
    x_recon, a 1D NumPy array of floats
    y_recon, a 1D NumPy array of floats
    sampled_elevation, a 2D NumPy array of floats
    reconstructed_elevation, a 2D NumPy array of floats

    Returns
    No explicit returns. Instead input argument
    reconstructed_elevation is modified so that it contains the estimates.
    """
    # Step through each point in the grid of reconstructed elevation estimates.
    for j_col, x_r in enumerate(x_recon):
        for i_row, y_r in enumerate(y_recon):

            # Find the Euclidean (as the crow flies) distance between the
            # point to estimate and all the samples.
            sample_positions = sampled_elevation[:, :2]
            position_differences = sample_positions - np.array([[x_r, y_r]])
            distance = np.sqrt(np.sum(position_differences ** 2, axis=1))

            # Pick out the the closest k samples and their distances.
            i_top_k = np.argsort(distance)[:k]
            elevation_top_k = sampled_elevation[i_top_k, 2]

            # Find the average of the k closest samples.
            reconstructed_elevation[i_row, j_col] = np.sum(elevation_top_k) / k


def visualize(x_recon, y_recon, reconstructed_elevation):
    """
    Turn the reconstructed elevation into a 3D plot.

    Arguments
    x_recon, a 1D NumPy array of floats
    y_recon, a 1D NumPy array of floats
    reconstructed_elevation, a 2D NumPy array of floats
    """
    fig = plt.figure()
    ax = fig.gca(projection='3d')

    # Expand the row and column locations to a full grid.
    X, Y = np.meshgrid(x_recon, y_recon)
    # Account for the convention that rows are numbered top to bottom.
    Y = np.flipud(Y)

    # Plot the surface, clean the axes, and add a colorbar.
    surf = ax.plot_surface(
        X, Y, reconstructed_elevation, cmap="turbo",
        linewidth=0, antialiased=True)
    ax.tick_params(bottom=False, top=False, left=False, right=False)
    ax.tick_params(
        labelbottom=False, labeltop=False, labelleft=False, labelright=False)
    fig.colorbar(surf, shrink=0.5, aspect=5)

    # fig.savefig(reconstructed_image_filename, dpi=300)
    plt.show()


if __name__ == "__main__":
    main()
